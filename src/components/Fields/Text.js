import React from "react";
import { Input, Label, Title } from "./style";

export default function Field({ label, name, value, onChange = { onChange } }) {
  return (
    <Label>
      <Title>{label}</Title>
      <Input
        value={value}
        type="text"
        onChange={(event) => onChange(event.target.value)}
      />
    </Label>
  );
}
