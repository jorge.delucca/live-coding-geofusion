import React from "react";
import { Input, Label, Title } from "./style";

export default function Number({
  label,
  name,
  value,
  onChange = { onChange },
}) {
  return (
    <Label>
      <Title>{label}</Title>
      <Input type="Number" onChange={(event) => onChange(event.target.value)} />
    </Label>
  );
}
