import styled, { css, createGlobalStyle } from "styled-components";

export const Label = styled.label`
  border: solid black 1px;
  padding: 0.5rem 0.5rem;
  width: 20rem;
  border-radius: 0.25rem;
  display: flex;
  flex-direction: column;
  margin-bottom: 1rem;
`;

export const Input = styled.input`
  border: 0;
  outline: 0;
  width: 100%;
`;

export const Title = styled.div`
  font-size: 0.75rem;
  font-weight: 300;
`;

export const Options = styled.div``;
