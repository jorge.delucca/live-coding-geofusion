import styled, { css, createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`

  :root {
    --box-shadow: 0 0.375rem 0.5rem 0 rgba(0, 0, 0, 0.14), 0 0.0625rem 1.125rem 0 rgba(0, 0, 0, 0.12), 0 0.1875rem 0.3125rem -0.125rem rgba(0, 0, 0, 0.3);
  }

  body {
    font-family: Lato;
    margin: 0;
    font-size: 16px;
    height: 100vh;
  }
  
  #app {
    display: flex;
    flex-direction: column;
    height: 100%;
    font-family: Lato;
  }
  
  h1, h2, h3 {
    margin: 0;
  }
  
  button: {
    font-family: inherit;
  }
`;

export const mainColor = "#00638C";

export const Header = styled.div`
  background: #999;
  color: #000;
  font-size: 0.75rem;
  height: 56px;

  ${(props) =>
    props.theme === "geofusion" &&
    css`
      background: ${mainColor};
      color: #fff;
    `}

  box-shadow: var(--box-shadow);
`;

export const Title = styled.h1`
  display: flex;
  align-items: center;
  height: 100%;
  padding-left: 2rem;
`;

export const Body = styled.div`
  padding: 1rem 2rem;
`;
