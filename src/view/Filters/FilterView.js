import React from "react";
import { Container } from "./style";
import Text from "../../components/Fields/Text";
import Number from "../../components/Fields/Number";
import Select from "../../components/Fields/Select";

const fields = [
  {
    label: "Nome",
    id: "name",
  },
  {
    label: "Endereço",
    id: "address",
  },
  {
    label: "Valor",
    id: "value",
  },
  {
    label: "Tipo",
    id: "type",
    options: [
      {
        label: "A",
        value: "a",
      },
      {
        label: "b",
        value: "b",
      },
      {
        label: "b",
        value: "b",
      },
    ],
  },
];

const fieldTypes = {
  name: "text",
  value: "number",
  address: "text",
  type: "select",
};

export default function Filters() {
  const [values, setValues] = React.useState({});

  function updateValue(name, value) {
    setValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  }

  return (
    <Container>
      {
        // Construir código aqui
      }
      <Text
        label="Exemplo texto"
        name="name"
        value={values["name"]}
        onChange={(value) => updateValue("name", value)}
      />
      <Number
        label="Exemplo numérico"
        name="number"
        value={values["number"]}
        onChange={(value) => updateValue("number", value)}
      />
      <Select
        label="Exemplo select"
        options={[]}
        name="select"
        value=""
        onChange={(value) => updateValue("select", value)}
      />
      {
        // Renderizar os campos acima por iteração da array
      }
      <h4>Valor dos estados:</h4>
      {Object.keys(values).map((key) => (
        <p key={key}>
          <strong>{key}</strong>: {values[key]}
        </p>
      ))}
    </Container>
  );
}
