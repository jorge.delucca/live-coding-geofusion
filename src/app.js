import React from "react";
import { Title, Header, GlobalStyles, Body } from "./styles";
import Filters from "./view/Filters/FilterView";

class App extends React.Component {
  render() {
    const theme = "geofusion";
    return (
      <>
        <GlobalStyles />
        <Header theme={theme}>
          <Title>Geofusion</Title>
        </Header>
        <Body>
          <Filters />
        </Body>
      </>
    );
  }
}

export default App;
