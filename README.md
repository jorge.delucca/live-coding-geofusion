# Exercício Live Coding Geofusion - Front-end

Projeto criado com create-react-app [Create React App](https://github.com/facebook/create-react-app).

## Execuções

Dentro do diretório, o comando

### `npm start`

Inicia o projeto em modo de desenvolvimento.
Abra [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

A página está com hot reload, então refletirá a cada alteração automaticamente.

# Sobre o exercício

Dentro da pasta /view existe o arquivo FilterView.js

No arquivo FilterView existe uma array `fields` e um objeto `fieldTypes`. `fields` é uma lista que deve ser iterada para que cada item seja renderizado como um elemento de formulário, porém o tipo dos itens não está especificado em seu próprio objeto. Para isso, você deve acessar `fieldTypes` e agregar a informação correspondente à lista de `fields`. Cada chave de `fieldTypes` é correspondente a um `id` de cada item de `fields`.

Feito isso, você deverá utilizar o resultado da array enriquecida com o `type` para iterar a mesma e retornar o componente adaquado para cada tipo.

`type: "text"` corresponde ao componente `<Text />`

`type: "number"` corresponde ao componente `<Number />`

`type: "select"` corresponde ao componente `<Select />`


Ao finalizar a iteração com os componentes, cada item de formulário já deve ter sido renderizado e seu `label` estar sendo exibido. Você pode conferir se `id` e `value` estão corretos ao alterar cada campo, fazendo essas informações serem impressas em "Valor dos estados"

Observe que o componente de `Select` não exibiu nenhuma opção. Isso é uma outra parte do exercício.

Utilizando os valores do item iterado do tipo `select`, construa o componente `<Select />`, fazendo com que ele retorne a lista de opções recebida em `options`. Você pode estilizar esse componente como quiser, seja por `css` ou por `styled-components`. Ao clicar em um dos itens renderizados, você deve emitir um `onChange`, para que esse evento seja emitido à view `FilterView`, fazendo com que esse evento seja dinâmicamente salvo em estado, assim como os outros campos já fazem.

É isso, bom exercício para você :)
